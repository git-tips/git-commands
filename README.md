# git-commands

### List ignored files
```
git ls-files . --ignored --exclude-standard --others
```
### List untracked files
```
git ls-files . --exclude-standard --others
```

### Delete a branch. The branch must be fully merged in its upstream branch, or in HEAD if no upstream was set with --track or --set-upstream-to.
```
git branch -d <branch>
```
### Check remote origin & current branch,head
```
git remote -v
git branch -v
```
### Changing the latest Git commit message after push
```
git commit --amend -m "New message"
git push --force
# Note that using --force is not recommended, since this changes the history of your repository.
```
### Show changes to files in the "staged" area
```
git diff --staged [file]
```
### Throw away local modifications
```
git checkout -f
```
### Copy and rename branch
```
git branch -c name name_copy
git branch -m old_name  new_name
```
### Rebase or rename commit
```
git rebase -i (HEAD~<N>|hash|--root)
```
### Ignore changes to files that are tracked
```
git update-index --skip-worktree FILENAME
# Note that Git does not provide a way to ignore changes to tracked files, so alternate solutions are recommended.
```
